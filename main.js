
//變數宣告//

var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var mode = "brush";

var brush = document.getElementById('brush');
var type = document.getElementById('type');
var save = document.getElementById('save');
var rec = document.getElementById('rec');
var circle = document.getElementById('round');
var tri = document.getElementById('tri');

var radius = 10;
var dragging = false;
var colorfixed = false;

var steps = -1;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

context.lineWidth = radius*2;

var clear = document.getElementById('delete');

var minRad = 1;
var maxRad = 99;
var radSpan = document.getElementById('radval');
var decrad = document.getElementById('decrad');
var addrad = document.getElementById('addrad');

var color = document.getElementById("colorpad");


var font = 20;
var minfont = 2;
var maxfont = 98;
var fontSpan = document.getElementById('fontval');
var decfont = document.getElementById('decfont');
var addfont = document.getElementById('addfont');

var eraser = document.getElementById('eraser');

var text = document.getElementById('text').value;
var typeface = document.getElementById('typeface_select').value;

var imageupload = document.getElementById('upload');

canvas.style.cursor = "url(brush_cursor.png), auto";

var undobtn = document.getElementById('undo');
var redobtn = document.getElementById('redo');
var array = new Array();
push();

//Brush//
function getMousepos(canvas, evt){
    var rect = canvas.getBoundingClientRect();
    return{
        X: ( evt.clientX - rect.left )/( rect.right - rect.left )* canvas.width,
        Y: ( evt.clientY - rect.top )/( rect.bottom - rect.top )* canvas.height
    };
}

var putPoint = function(evt){
    let pos = getMousepos(canvas, evt);
    context.lineWidth = radius*2;
    if(dragging){
        context.lineTo(pos.X, pos.Y);
        context.stroke();
        context.beginPath();
        context.arc(pos.X, pos.Y, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(pos.X, pos.Y);
    }
}

var engage = function(evt){
    if(mode == "brush"){
        dragging = true;
        putPoint(evt);
    }
    else if(mode == "text"){
        putText(evt);
    }
    else if(mode == "rec"){
        putRec(evt);
    }
    else if(mode == "circle"){
        putCircle(evt);
    }
    else if(mode == "tri"){
        dragging = true;
        putTri(evt);
    }
}

var disengage = function(){
    dragging = false;
    context.beginPath();
    push();
}

var disengage2 = function(){
    dragging = false;
    context.beginPath();
}

//Rec
function putRec(evt){
    let pos = getMousepos(canvas, evt);
    context.lineWidth = radius*2;
    context.fillRect(pos.X, pos.Y, 0, 0);
    context.strokeRect(pos.X, pos.Y, radius, radius);
}


//Circle
function putCircle(evt){
    let pos = getMousepos(canvas, evt);
    context.lineWidth = radius*2;
    context.arc(pos.X, pos.Y, radius, 0, Math.PI*2);
    context.fill();
}

//Tri
function putTri(evt){
    let pos = getMousepos(canvas, evt);
    context.lineWidth = radius*2;
    if(dragging){
        context.beginPath();
        context.moveTo(pos.X, pos.Y);
        context.lineTo(pos.X-radius*4, pos.Y+radius*4);
        context.lineTo(pos.X+radius*4, pos.Y+radius*4);
        context.fill();
    }
}

//Clear//
function cl(){
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//Radcontrol//
var setRadius = function(newRadius){
    if(newRadius < minRad) newRadius = minRad;
    else if(newRadius > maxRad) newRadius = maxRad;
    radius = newRadius;
    context.lineWidth = radius*2;
    radSpan.innerHTML = radius;
}

//Change color//
var setcolor = function (){
    if(!colorfixed){
        context.fillStyle = this.value;
        context.strokeStyle = this.value;
    }
}

//字大小//
var setfont = function(newfont){
    if(newfont < minfont) newfont = minfont;
    else if(newfont > maxfont) newfont = maxfont;
    font = newfont;
    fontSpan.innerHTML = newfont;
}

//Erase//
function erase(){
    mode = "brush";
    context.fillStyle = "#FFFFFF";
    context.strokeStyle = "#FFFFFF";
    canvas.style.cursor = "url(erase_cursor.png), auto";
    colorfixed = true;
}

//Type//
var putText = function(evt){
    text = document.getElementById('text').value;
    typeface = document.getElementById('typeface_select').value;
    let pos = getMousepos(canvas, evt);
    context.font = font + "px " + typeface;
    context.fillText(text, pos.X, pos.Y);
}

//Download//
function downloadCanvas(link, filename){
    link.href = canvas.toDataURL();
    link.download = filename;
}

//Upload//
function paste(evt){
    var loader = new FileReader();
    loader.onload = function(evt){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            img.width = 
            context.drawImage(img, 0, 0);
        }
        img.src = evt.target.result;
    }
    loader.readAsDataURL(evt.target.files[0]);
    mode = "brush";
    radius = 10;
    canvas.style.cursor = "url(brush_cursor.png), auto";
}

//Redo Undo//
function push(){
    steps ++;
    if(steps < array.length){
        array.length = steps;
    }
    array.push(canvas.toDataURL());
    console.log(steps);
}

function undo(){
    if(steps > 0){
        steps--;
        var Pic = new Image();
        Pic.src = array[steps];
        cl();
        Pic.onload = function(){
            context.drawImage(Pic, 0, 0);
        }
    }
}

function redo(){
    if(steps < array.length-1){
        steps++;
        var Pic = new Image();
        Pic.src = array[steps];
        cl();
        Pic.onload = function(){
            context.drawImage(Pic, 0, 0, canvas.width, canvas.height);
        }
    }
}

//監聽//
canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mouseup', disengage);
canvas.addEventListener('mouseout', disengage2);

clear.addEventListener('click', cl);

decrad.addEventListener('click', function(){
    setRadius(radius-1);
});

addrad.addEventListener('click', function(){
    setRadius(radius+1);
});

color.addEventListener('change', setcolor);

decfont.addEventListener('click', function(){
    setfont(font-2);
});

addfont.addEventListener('click', function(){
    setfont(font+2);
});

eraser.addEventListener('click', erase);

type.addEventListener('click', function(){
    context.fillStyle = "#000000";
    context.strokeStyle = "#000000";
    mode = "text";
    canvas.style.cursor = "url(text_cursor.png), auto";
    colorfixed = false;
});

brush.addEventListener('click', function(){
    context.fillStyle = "#000000";
    context.strokeStyle = "#000000";
    mode = "brush";
    canvas.style.cursor = "url(brush_cursor.png), auto";
    colorfixed = false;
});

save.addEventListener('click', function(){
    downloadCanvas(this, Math.random().toString(36).substr(2,9));
});

imageupload.addEventListener('change', paste);

undobtn.addEventListener('click', undo);

redobtn.addEventListener('click', redo);

rec.addEventListener('click', function(){
    context.fillStyle = "#000000";
    context.strokeStyle = "#000000";
    mode = "rec";
    canvas.style.cursor = "move";
    colorfixed = false;
});

circle.addEventListener('click', function(){
    context.fillStyle = "#000000";
    context.strokeStyle = "#000000";
    mode = "circle";
    canvas.style.cursor = "move";
    colorfixed = false;
});

tri.addEventListener('click', function(){
    context.fillStyle = "#000000";
    context.strokeStyle = "#000000";
    mode = "tri";
    canvas.style.cursor = "move";
    colorfixed = false;
});
